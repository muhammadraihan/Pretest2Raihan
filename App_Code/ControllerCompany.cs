﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerCompany : ClassBase
{
    public ControllerCompany(DataClassesDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Company[] Data()
    {
        return db.Companies.ToArray();
    }

    public Company Create(string name, string address, string email, string telephone)
    {
        Company company = new Company
        {
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Flag = 1,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };
        db.Companies.InsertOnSubmit(company);
        return company;
    }

    public Company Cari(string UID)
    {
        return db.Companies.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public Company Update(string UID, string name, string address, string email, string telephone)
    {
        var company = Cari(UID);
        if (company != null)
        {
            company.Name = name;
            company.Address = address;
            company.Email = email;
            company.Telephone = telephone;
            company.Flag = 1;
            company.CreatedBy = 1;

            return company;
        }
        else
        {
            return null;
        }
    }

    public Company Delete(string UID)
    {
        var company = Cari(UID);
        if (company != null)
        {
            //db.Table_Users.DeleteAllOnSubmit(company.Table_Users);
            //db.Table_Documents.DeleteAllOnSubmit(company.Table_Documents);
            db.Companies.DeleteOnSubmit(company);
            db.SubmitChanges();
            return company;
        }
        else
        {
            return null;
        }
    }

    public void DropDownListCompany(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });

    }

    public ListItem[] DropDownList()
    {
        List<ListItem> company = new List<ListItem>();

        company.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        company.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return company.ToArray();
    }
}