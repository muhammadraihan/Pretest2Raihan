﻿using AjaxControlToolkit.HtmlEditor.ToolbarButtons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.PeerToPeer;
using System.Net;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.Users.Select(x => new
        {
            x.ID,
            x.UID,
            x.Name,
            NameCompany = x.Company.Name,
            NamePosition = x.Position.Name,
            x.Address,
            x.Email,
            x.Telephone

        }).ToArray();
    }

    public User Create(int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        User tBUser = new User
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDPosition = IDPosition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = password,
            Role = "Admin",
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.Users.InsertOnSubmit(tBUser);
        return tBUser;
    }

    public User Cari(int ID)
    {
        return db.Users.FirstOrDefault(x => x.ID == ID);

    }
    public User Update(int ID, int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        var user = Cari(ID);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.IDPosition = IDPosition;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;
            user.Username = username;
            user.Password = password;
            user.Role = "Admin";
            user.Flag = 1;
            user.CreatedBy = 1;


            return user;
        }
        else
        {
            return null;
        }
    }

    public User Delete(int ID)
    {
        var user = Cari(ID);
        if (user != null)
        {
            db.Users.DeleteOnSubmit(user);
            db.SubmitChanges();
            return user;
        }
        else
        {
            return null;
        }
    }
}