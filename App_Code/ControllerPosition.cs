﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerPosition
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(DataClassesDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Position[] Data()
    {
        return db.Positions.ToArray();
    }

    public Position Create(string name)
    {
        Position position = new Position
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };
        db.Positions.InsertOnSubmit(position);
        return position;
    }

    public Position Cari(string UID)
    {
        return db.Positions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public Position Update(string UID, string name)
    {
        var position = Cari(UID);
        if (position != null)
        {
            position.Name = name;
            position.CreatedBy = 1;

            return position;
        }
        else
        {
            return null;
        }
    }

    public Position Delete(string UID)
    {
        var brand = Cari(UID);
        if (brand != null)
        {
            db.Positions.DeleteOnSubmit(brand);
            db.SubmitChanges();
            return brand;
        }
        else
        {
            return null;
        }
    }

    public void DropDownListPosition(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });

    }

    public ListItem[] DropDownList()
    {
        List<ListItem> position = new List<ListItem>();

        position.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        position.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return position.ToArray();
    }
}