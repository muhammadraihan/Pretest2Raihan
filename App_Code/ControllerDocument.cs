﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControllerDocument
/// </summary>
public class ControllerDocument : ClassBase
{
    public ControllerDocument(DataClassesDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.Documents.Select(x => new
        {
            x.ID,
            x.UID,
            x.Name,
            NameCompany = x.Company.Name,
            NameCategory = x.DocumentCategory.Name,
            x.Description
            

        }).ToArray();
    }

    public Document Create(int IDCompany, int IDCategory, string name, string description)
    {
        Document document = new Document
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDCategory = IDCategory,
            Name = name,
            Description = description,
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.Documents.InsertOnSubmit(document);
        return document;
    }
    public Document Cari(int ID)
    {
        return db.Documents.FirstOrDefault(x => x.ID == ID);
    }

    public Document Update(int ID, int IDCompany, int IDCategory, string name, string description)
    {
        var document = Cari(ID);

        if (document != null)
        {
            document.IDCompany = IDCompany;
            document.IDCategory = IDCategory;
            document.Name = name;
            document.Description = description;
            document.Flag = 1;
            document.CreatedBy = 1;


            return document;
        }
        else
        {
            return null;
        }
    }
    public Document Delete(int ID)
    {
        var document = Cari(ID);
        if (document != null)
        {
            db.Documents.DeleteOnSubmit(document);
            db.SubmitChanges();
            return document;
        }
        else
        {
            return null;
        }
    }
}