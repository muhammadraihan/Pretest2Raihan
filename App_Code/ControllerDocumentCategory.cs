﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerDocumentCategory
/// </summary>
public class ControllerDocumentCategory : ClassBase
{
    public ControllerDocumentCategory(DataClassesDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DocumentCategory[] Data()
    {
        return db.DocumentCategories.ToArray();
    }

    public DocumentCategory Create(string name)
    {
        DocumentCategory documentCategory = new DocumentCategory
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };
        db.DocumentCategories.InsertOnSubmit(documentCategory);
        return documentCategory;
    }

    public DocumentCategory Cari(string UID)
    {
        return db.DocumentCategories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public DocumentCategory Update(string UID, string name)
    {
        var documentcategory = Cari(UID);
        if (documentcategory != null)
        {
            documentcategory.Name = name;
            documentcategory.CreatedBy = 1;

            return documentcategory;
        }
        else
        {
            return null;
        }
    }

    public DocumentCategory Delete(string UID)
    {
        var category = Cari(UID);
        if (category != null)
        {
            db.DocumentCategories.DeleteOnSubmit(category);
            db.SubmitChanges();
            return category;
        }
        else
        {
            return null;
        }
    }

    public void DropDownListCompany(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });

    }

    public ListItem[] DropDownList()
    {
        List<ListItem> category = new List<ListItem>();

        category.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        category.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return category.ToArray();
    }
}