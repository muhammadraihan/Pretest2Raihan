﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
       
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                repeaterUser.DataSource = db.Users.Select(x => new
                {
                    x.ID,
                    x.UID,
                    x.Name,
                    NameCompany = x.Company.Name,
                    NamePosition = x.Position.Name,
                    x.Address,
                    x.Email,
                    x.Telephone

                }).ToArray();
                repeaterUser.DataBind();
            }
    }
    

    protected void repeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Form.aspx?id=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var user = db.Users.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());

                db.Users.DeleteOnSubmit(user);
                db.SubmitChanges();

                LoadData();
            }
        }
    }

    
}