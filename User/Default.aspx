﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="User_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    <h1>List User</h1>
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="Form.aspx" class="btn btn-success btn-sm" id="button-add">Add User</a>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Company</th>
                                <th>Position</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <asp:Repeater ID="repeaterUser" runat="server" OnItemCommand="repeaterUser_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td class="fitSize"><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("NameCompany") %></td>
                                        <td><%# Eval("NamePosition") %></td>
                                        <td><%# Eval("Name") %></td>
                                        <td><%# Eval("Address") %></td>
                                        <td><%# Eval("Email") %></td>
                                        <td><%# Eval("Telephone") %></td>
                                        <td>
                                            <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="btn btn-info btn-sm" Text="Update" CommandArgument='<%# Eval("ID") %>'/>
                                            <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btn btn-danger btn-sm" Text="Delete" CommandArgument='<%# Eval("ID") %>'/>

                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

