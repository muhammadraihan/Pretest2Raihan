USE [master]
GO
/****** Object:  Database [Pretest]    Script Date: 04/08/2023 20:59:16 ******/
CREATE DATABASE [Pretest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Pretest', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Pretest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Pretest_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Pretest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [Pretest] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Pretest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Pretest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Pretest] SET ARITHABORT OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Pretest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Pretest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Pretest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Pretest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Pretest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Pretest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Pretest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Pretest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Pretest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Pretest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Pretest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Pretest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Pretest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Pretest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Pretest] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Pretest] SET  MULTI_USER 
GO
ALTER DATABASE [Pretest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Pretest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Pretest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Pretest] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Pretest] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Pretest] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Pretest] SET QUERY_STORE = ON
GO
ALTER DATABASE [Pretest] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [Pretest]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](50) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Document]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentCategory]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_DocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_Company] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_Company]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentCategory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[DocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentCategory]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company1] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company1]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Position] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[Position] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Position]
GO
/****** Object:  StoredProcedure [dbo].[sp_createCompany]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createCompany]
	-- Add the parameters for the stored procedure here

	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Company]
           ([UID]
           ,[Name]
           ,[Address]
		   ,[Email]
           ,[Telephone]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
 
     VALUES
           (NEWID()
           ,@name
           ,@address
		   ,@email
		   ,@telephone
		   ,@flag
		   ,@iduser
	       ,GETDATE())
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocument]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocument]
	@iduser int,
	@idcompany int,
	@idcategory int,
	@name varchar(100),
	@description text,
	@flag int,
	@createby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Document]
           ([IDCompany]
		   ,[IDcategory]
           ,[UID]
           ,[Name]
           ,[Description]
           ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (@idcompany,
		   @idcategory,
		   NEWID(),
		   @name,
		   @description,
		   @flag,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocumentCategory]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocumentCategory]
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[DocumentCategory]
           ([UID]
           ,[Name]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID(),
		   @name,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createPosition]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createPosition]
	@iduser int,
	@name varchar(100),
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Position]
           ([UID]
           ,[Name]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (
		   NEWID(),
		   @name,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createUser]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createUser]
	-- Add the parameters for the stored procedure here

	@idcompany int,
	@idposition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[User]
           ([UID]
		   ,[IDCompany]
		   ,[IDPosition]
           ,[Name]
           ,[Address]
		   ,[Email]
           ,[Telephone]
		   ,[Username]
		   ,[Password]	
		   ,[Role]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
 
     VALUES
           (NEWID()
		   ,@idcompany
		   ,@idposition
           ,@name
           ,@address
		   ,@email
		   ,@telephone
		   ,@username
		   ,CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2) 
		   ,@role
		   ,@flag
		   ,@iduser
	       ,GETDATE())
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteCompany]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteCompany]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Company WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDocument]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteDocument]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Document WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDocumentCategory]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteDocumentCategory]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM DocumentCategory WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deletePosition]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deletePosition]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Position WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteUser]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteUser]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [User] WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
@email varchar(50),
@password varchar(50),
@retVal int OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM [User]
	WHERE
		[Email] = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateCompany]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateCompany]
	-- Add the parameters for the stored procedure here

	@id int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update [dbo].[Company] SET
   
           [Name] = @name
           ,[Address] = @address
		   ,[Email] = @email
           ,[Telephone] =@telephone
		   ,[Flag] = @flag
		   ,[CreatedBy] = @iduser
 
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocument]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocument]
	@id int,
	@iduser int,
	@idcompany int,
	@idcategory int,
	@name varchar(100),
	@description text,
	@flag int,
	@createby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Document]
   SET [IDCompany] = @idcompany
      ,[Name] = @name
	  ,[IDCategory]=@idcategory
      ,[description] = @description
      ,[Flag] = @flag
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocumentCategory]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocumentCategory]
	@id int,
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[DocumentCategory]
   SET [Name] = @name
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updatePosition]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updatePosition]
	@id int,
	@iduser int,
	@name varchar(100),
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Position]
   SET [Name] = @name
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateUser]    Script Date: 04/08/2023 20:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateUser]
	-- Add the parameters for the stored procedure here

	@idcompany int,
	@idposition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update [dbo].[User] SET
           
		   [IDCompany] = @idcompany
		   ,[IDPosition] = @idposition
           ,[Name] = @name
           ,[Address] = @address
		   ,[Email]	= @email
           ,[Telephone]	= @telephone
		   ,[Username]	= @username
		   ,[Password]	= @password
		   ,[Role] = @role
		   ,[Flag] = @flag

 
    WHERE ID = @iduser
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
USE [master]
GO
ALTER DATABASE [Pretest] SET  READ_WRITE 
GO
