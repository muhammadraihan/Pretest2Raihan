﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            repeaterDocument.DataSource = db.Documents.Select(x => new
            {
                x.ID,
                x.UID,
                x.Name,
                NameCompany = x.Company.Name,
                NameCategory = x.DocumentCategory.Name,
                x.Description

            }).ToArray();
            repeaterDocument.DataBind();
        }
    }

    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Form.aspx?id=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var document = db.Documents.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());

                db.Documents.DeleteOnSubmit(document);
                db.SubmitChanges();

                LoadData();
            }
        }
    }
}