﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                ControllerDocumentCategory category = new ControllerDocumentCategory(db);
                var cat = category.Cari(Request.QueryString["uid"]);

                if (cat != null)
                {
                    InputCategory.Text = cat.Name;

                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update Category";

                }
                else
                {
                    ButtonOk.Text = "Add New";
                    LabelTitle.Text = "Add New Category";
                }

            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                ControllerDocumentCategory category = new ControllerDocumentCategory(db);
                if (ButtonOk.Text == "Add New")
                {
                    category.Create(InputCategory.Text);

                }

                else if (ButtonOk.Text == "Update")
                {
                    category.Update(Request.QueryString["uid"], InputCategory.Text);
                }

                db.SubmitChanges();

                Response.Redirect("/DocumentCategory/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/DocumentCategory/Default.aspx");
    }

    
}