﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                LoadData(db);
            }
        }

    }

    private void LoadData(DataClassesDataContext db)
    {
        ControllerDocumentCategory position = new ControllerDocumentCategory(db);
        repeaterCategory.DataSource = position.Data();
        repeaterCategory.DataBind();
    }


    protected void repeaterCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/DocumentCategory/Form.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var category = db.DocumentCategories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.DocumentCategories.DeleteOnSubmit(category);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }
}